import * as React from 'react';

import {AuthContext} from 'src/utils/auth-context';
import {useTranslation} from 'react-i18next';
import {
  StyleSheet,
  View,
  ScrollView,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  Switch,
} from 'react-native';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import Input from 'src/components/Input';
import RadioInput from 'src/components/RadioInput';
import ThemeView from 'src/components/ThemeView';
import {useNavigation} from '@react-navigation/native';
import {CheckBox} from 'react-native-elements';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

function CategorySelection(props) {
  const {t} = useTranslation();
  const [categorydata, setCategorydata] = React.useState([]);
  const [value, setValue] = React.useState();

  const navigation = useNavigation();
  const {signUp, isLoading, theme} = React.useContext(AuthContext);
  const urlImage =
    theme === 'dark'
      ? require('src/assets/images/login_dark.png')
      : require('src/assets/images/login.png');

  React.useEffect(() => {
    let data = [
      {id: 1, label: 'Food', checked: false, value: 'FOOD'},
      {id: 2, label: 'Grocery', checked: true, value: 'GROCERY'},
    ];
    setCategorydata(data);
  }, []);
  console.log(' props.route.params ', props.route.params);
  const submit = () => {
    navigation.navigate('AddStoreTimings',{...props.route.params,category:value});
    // navigation.navigate('Home',{...props.route.params,category:value});
  };

  return (
    <ThemeView
      Component={KeyboardAvoidingView}
      behavior="padding"
      style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar hidden />
        <View style={styles.content}>
          <Text medium h1 style={styles.text}>
            {t('signup:Select Root Category')}
          </Text>
          {categorydata &&
            categorydata.map((data, i) => {
              return (
                <CheckBox
                  title={data.label}
                  checked={value == data.value ? true : false}
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  onPress={() => {
                    setValue(data.value);
                  }}
                  key={i}
                />
              );
            })}
          <Button
            loading={isLoading}
            title={t('signup: Finish')}
            // onPress={() => signUp({username, password})}
            onPress={() => submit()}
            containerStyle={styles.button}
          />
          <Text medium h5 style={styles.text}>
            Already registered click here to login
          </Text>
        </View>
      </ScrollView>
    </ThemeView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginTop: 50,
    paddingHorizontal: 20,
  },
  image: {
    marginVertical: 40,
    marginHorizontal: 26,
  },
  text: {
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
  },
  button: {
    marginVertical: 15,
  },
});

export default CategorySelection;

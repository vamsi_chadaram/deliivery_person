import * as React from 'react';
import {AuthContext} from 'src/utils/auth-context';
import {useTranslation} from 'react-i18next';
import {
  StyleSheet,
  View,
  ScrollView,
  StatusBar,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import Input from 'src/components/Input';
import ThemeView from 'src/components/ThemeView';
import {useNavigation} from '@react-navigation/native';

function SignUpScreen() {
  const {t} = useTranslation();
  const [username, setUsername] = React.useState('');
  const [firstname, setFirstname] = React.useState('');
  const [lastname, setLastname] = React.useState('');
  const [storename, setStorename] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [contact, setContact] = React.useState('');
  const navigation = useNavigation();
  const {signUp, isLoading, theme} = React.useContext(AuthContext);
  const urlImage =
    theme === 'dark'
      ? require('src/assets/images/login_dark.png')
      : require('src/assets/images/login.png');

  const submit = () => {
    navigation.navigate('CategorySelection', {
      username: username,
      password: password,
      storename: storename,
      firstname: firstname,
      lastname: lastname,
      contact: contact,
    });
  };

  return (
    <ThemeView
      Component={KeyboardAvoidingView}
      behavior="padding"
      style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar hidden />
        <View style={styles.content}>
          <Text medium h1 style={styles.text}>
            {t('signup:text_signup')}
          </Text>
          <Input
            label={t('inputs:text_store_name')}
            value={storename}
            onChangeText={setStorename}
            secondary
          />
          <Input
            label={t('inputs:text_first_name')}
            value={firstname}
            onChangeText={setFirstname}
            secondary
          />
          <Input
            label={t('inputs:text_last_name')}
            value={lastname}
            onChangeText={setLastname}
            secondary
          />
          <Input
            label={t('inputs:text_email')}
            value={username}
            onChangeText={setUsername}
            secondary
          />
          <Input
            label={t('inputs:text_password')}
            value={password}
            onChangeText={setPassword}
            secureTextEntry
            secondary
          />
          <Input
            label={t('inputs:text_contact_number')}
            value={contact}
            onChangeText={setContact}
            secondary
          />
          <Button
            loading={isLoading}
            title={t('signup:text_button_next')}
            // onPress={() => signUp({username, password,contact})}
            onPress={() => submit()}
            containerStyle={styles.button}
          />
          <Text medium h5 style={styles.text}>
            Already registered click here to login
          </Text>
        </View>
      </ScrollView>
    </ThemeView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginTop: 50,
    paddingHorizontal: 20,
  },
  image: {
    marginVertical: 40,
    marginHorizontal: 26,
  },
  text: {
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
  },
  button: {
    marginVertical: 15,
  },
});

export default SignUpScreen;

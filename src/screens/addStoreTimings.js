import * as React from 'react';

import {AuthContext} from 'src/utils/auth-context';
import {useTranslation} from 'react-i18next';
import {
  StyleSheet,
  View,
  ScrollView,
  StatusBar,
  Picker,
  Image,
  KeyboardAvoidingView,
  Switch,
} from 'react-native';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import ThemeView from 'src/components/ThemeView';
import {useNavigation} from '@react-navigation/native';

function AddStoreTimings(props) {
  const {t} = useTranslation();
  const [timedata, setTimedata] = React.useState([]);
  const [hours, setHours] = React.useState('10');
  const [meridiem, setMeridiem] = React.useState('AM');
  const navigation = useNavigation();
  const {signUp, isLoading, theme} = React.useContext(AuthContext);
  const urlImage =
    theme === 'dark'
      ? require('src/assets/images/login_dark.png')
      : require('src/assets/images/login.png');

  React.useEffect(() => {
    let data = [
      {id: 1, label: '1', value: '1'},
      {id: 2, label: '2', value: '2'},
      {id: 2, label: '3', value: '3'},
      {id: 2, label: '4', value: '4'},
      {id: 2, label: '5', value: '5'},
      {id: 2, label: '6', value: '6'},
      {id: 2, label: '7', value: '7'},
      {id: 2, label: '8', value: '8'},
      {id: 2, label: '9', value: '9'},
      {id: 2, label: '10', value: '10'},
      {id: 2, label: '11', value: '11'},
      {id: 2, label: '12', value: '12'},
    ];
    setTimedata(data);
  }, []);

  const submit = () => {
    navigation.navigate('Home', {hours: hours, meridiem: meridiem});
  };

  return (
    <ThemeView
      Component={KeyboardAvoidingView}
      behavior="padding"
      style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar hidden />
        <View style={styles.content}>
          <Text medium h1 style={styles.text}>
            {t('signup:Add Store Timings')}
          </Text>

          <Text medium h3 style={styles.text}>
            {t('signup:Select Time')}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Picker
              selectedValue={hours}
              style={{height: 50, width: 150}}
              onValueChange={(itemValue, itemIndex) => setHours(itemValue)}>
              <Picker.Item label="Select" value="" />
              {timedata &&
                timedata.map((data, i) => {
                  return (
                    <Picker.Item
                      label={data.label}
                      key={i}
                      value={data.value}
                    />
                  );
                })}
            </Picker>
            <Picker
              selectedValue={meridiem}
              style={{height: 50, width: 150}}
              onValueChange={(itemValue, itemIndex) => setMeridiem(itemValue)}>
              <Picker.Item label="Select" value="" />
              <Picker.Item label="AM" value="AM" />
              <Picker.Item label="PM" value="PM" />
            </Picker>
          </View>

          <Button
            loading={isLoading}
            title={t('signup: Finish')}
            onPress={() => submit()}
            containerStyle={styles.button}
          />
          <Text medium h5 style={styles.text}>
            Already registered click here to login
          </Text>
        </View>
      </ScrollView>
    </ThemeView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginTop: 50,
    paddingHorizontal: 20,
  },
  image: {
    marginVertical: 40,
    marginHorizontal: 26,
  },
  text: {
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
  },
  button: {
    marginVertical: 15,
  },
});

export default AddStoreTimings;
